#ifndef MOTOR_CONTROLLER_HPP
#define MOTOR_CONTROLLER_HPP
#include "Arduino.h"

class MotorController
{
private:
    int enA, enB, in1, in2, in3, in4;
    int currentSpeed;
    double motorCorrectionA, motorCorrectionB;

public:
    MotorController(const int& in1, const int& in2, const int& in3, const int& in4, const int& enA, const int& enB, const double& motorCorrectionA, const double& motorCorrectionB);
    void moveStraight(int speed, int minSpeed);
};

#endif
