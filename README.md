# Self Balancing Robot #

An Arduino program for controlling a self balancing bot with PID.

### What is this repository for? ###

*This project is created as part of a university course project. Further improvements may be made to the code.*
*Version: 0.1b*

### How to setup the code ###

*  The following components are required to use the code:
	*  Arduino (Uno used in this project)
	*  MPU6050 (GY-521 Breakout Board)
	*  L298N Motor Driver
	*  9V DC Motors
	*  Power supply for the Uno & driver (12V LiPo used)

This project uses classes to perform calculations for the PID, processing data from MPU, & controlling the motors.
It uses the I2CDev library from Jeff Rowberg.

### Contact ###

If you want to contribute to the project, feel free to fork it & submit pull requests, they will be reviewed
whenever I have time to check on the repo.

This project is licensed under GPLv2.0.