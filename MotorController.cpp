#include "MotorController.h"
#include "Arduino.h"

MotorController::MotorController(const int& in1, const int& in2, const int& in3, const int& in4, const int& enA, const int& enB, const double& motorCorrectionA, const double& motorCorrectionB)
{
    this -> in1 = in1;
    this -> in2 = in2;
    this -> in3 = in3;
    this -> in4 = in4;

    this -> enA = enA;
    this -> enB = enB;

    this -> motorCorrectionA = motorCorrectionA;
    this -> motorCorrectionB = motorCorrectionB;

    pinMode(in1, OUTPUT);
    pinMode(in2, OUTPUT);
    pinMode(in3, OUTPUT);
    pinMode(in4, OUTPUT);

    pinMode(enA, OUTPUT);
    pinMode(enB, OUTPUT);
}

void MotorController::moveStraight(int speed, int minSpeed)
{
    int direction;

    if(speed < 0){
        direction = -1;
        speed = min(speed, direction * minSpeed);
        speed = max(speed, -255);
    }
    else{
        direction = 1;
        speed = max(speed, direction * minSpeed);
        speed = min(speed, 255);
    }

    if(speed == currentSpeed) return;

    digitalWrite(in1, direction > 0 ? HIGH : LOW);
    digitalWrite(in2, direction > 0 ? LOW : HIGH);
    digitalWrite(in3, direction > 0 ? HIGH : LOW);
    digitalWrite(in4, direction > 0 ? LOW : HIGH);

    analogWrite(enA, abs(speed) * motorCorrectionA);
    analogWrite(enB, abs(speed) * motorCorrectionB);

    currentSpeed = speed;
}
